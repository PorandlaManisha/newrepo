﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeApp
{
    class Employee
    {
        public static int counter = 0;
        public Employee ()
        {
            this.Employeeid = counter++;
        }
        public int Employeeid { get; set; }
        public string EmployeeName { get; set; }
        public string DepartmentName { get; set; }
        public string DateOfBirth { get; set; }
        public string  Address{ get; set; }
        public int Salary { get; set; }
        public string Emailid { get; set; }


    }
}
